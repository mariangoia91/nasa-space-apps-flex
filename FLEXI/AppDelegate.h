//
//  AppDelegate.h
//  FLEXI
//
//  Created by Marian Goia on 23/04/16.
//  Copyright © 2016 Marian Goia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

