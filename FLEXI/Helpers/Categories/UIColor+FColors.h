//
//  UIColor+FColors.h
//  FLEXI
//
//  Created by Marian Goia on 23/04/16.
//  Copyright © 2016 Marian Goia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (FColors)

+ (UIColor *)f_orange;

+ (NSArray *)f_backgroundGradientColors;

@end
