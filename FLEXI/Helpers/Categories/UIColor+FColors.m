//
//  UIColor+FColors.m
//  FLEXI
//
//  Created by Marian Goia on 23/04/16.
//  Copyright © 2016 Marian Goia. All rights reserved.
//

#import "UIColor+FColors.h"

@implementation UIColor (FColors)

+ (UIColor *)f_orange {
    return [UIColor colorWithRed:1.00 green:0.37 blue:0.12 alpha:1.00];
}

+ (NSArray *)f_backgroundGradientColors {
    return [NSArray arrayWithObjects:
            (id)[UIColor colorWithRed:0.01 green:0.25 blue:0.51 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.02 green:0.29 blue:0.52 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.06 green:0.35 blue:0.58 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.14 green:0.37 blue:0.60 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.14 green:0.38 blue:0.58 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.17 green:0.39 blue:0.60 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.14 green:0.38 blue:0.58 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.14 green:0.37 blue:0.60 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.06 green:0.35 blue:0.58 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.02 green:0.29 blue:0.52 alpha:1.00].CGColor,
            (id)[UIColor colorWithRed:0.01 green:0.25 blue:0.51 alpha:1.00].CGColor,
            nil];
}

@end
