//
//  main.m
//  FLEXI
//
//  Created by Marian Goia on 23/04/16.
//  Copyright © 2016 Marian Goia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
