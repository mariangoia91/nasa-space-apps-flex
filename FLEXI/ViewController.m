//
//  ViewController.m
//  FLEXI
//
//  Created by Marian Goia on 23/04/16.
//  Copyright © 2016 Marian Goia. All rights reserved.
//

#import "ViewController.h"

#import "UIColor+FColors.h"

#import <EFCircularSlider/EFCircularSlider.h>
#import <SVProgressHUD/SVProgressHUD.h>

@import LocalAuthentication;

typedef enum {
    kNeckMuscleGroupTag = 1,
    kShouldersMuscleGroupTag = 2,
    kBicepsMuscleGroupTag = 3,
    kTricepsMuscleGroupTag = 4,
    kChestMuscleGroupTag = 5,
    kAbsMuscleGroupTag = 6,
    kUpperLegsMuscleGroupTag = 7,
    kLowerLegsMuscleGroupTag = 8,
} FTagForMuscleGroupButton;

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *muscleGroupImageView;

@property (nonatomic, weak) IBOutlet UIView *circularProgressViewContainer;

@property (nonatomic, strong) EFCircularSlider *circularSlider;
@property (nonatomic, weak) IBOutlet UILabel *forceLabel;

@property (nonatomic, weak) IBOutlet UILabel *welcomeLabel;

@property (nonatomic, assign) BOOL isAuthenticated;

- (IBAction)muscleGroupButtonPressed:(UIButton *)sender;

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupGradientBackground];
    [self setupCircularProgressView];
    
    [self setupImageViewChangeAnimation];
    
    [self checkLocalAuthenticationStatus];
}

#pragma mark - Circular Slider Action

- (void)valueChanged:(EFCircularSlider *)sender {
    [self.forceLabel setText:[NSString stringWithFormat:@"%.1f %% \n %.2f kg/f", sender.currentValue, [self kgForceForPercentage:sender.currentValue]]];
}

#pragma mark - View seutp helpers

- (void)setupCircularProgressView {
    EFCircularSlider *circularSlider = [[EFCircularSlider alloc] initWithFrame:CGRectMake(0, 0, self.circularProgressViewContainer.frame.size.width, self.circularProgressViewContainer.frame.size.height)];
    
    [circularSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    
    _circularSlider = circularSlider;
    
    [self.circularProgressViewContainer addSubview:_circularSlider];
    
    _circularSlider.minimumValue = 1.f;
    _circularSlider.maximumValue = 100.f;
    
    _circularSlider.lineWidth = 15.f;

    _circularSlider.handleColor = [UIColor redColor];
    
    _circularSlider.unfilledColor = [UIColor whiteColor];
    _circularSlider.filledColor = [UIColor blueColor];
    
    _circularSlider.handleType = bigCircle;
    
    [_circularSlider setBackgroundColor:[UIColor clearColor]];
    
    [self.circularSlider setCurrentValue:0.f];
    [self.forceLabel setText:[NSString stringWithFormat:@"%.1f %% \n %.2f kg/f", 0.f, [self kgForceForPercentage:0.f]]];
}

- (void)setupGradientBackground {
    CAGradientLayer *backgroundLayer = [self darkBlueAndWhiteGradientLayer];
    backgroundLayer.frame = self.view.frame;
    [self.view.layer insertSublayer:backgroundLayer atIndex:0];
}

- (CAGradientLayer *)darkBlueAndWhiteGradientLayer {
    NSArray *gradientColors = [UIColor f_backgroundGradientColors];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = gradientColors;
    
    return gradientLayer;
}

- (void)setupImageViewChangeAnimation {
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.muscleGroupImageView.layer addAnimation:transition forKey:nil];
}

#pragma mark - Utility

- (CGFloat)kgForceForPercentage:(CGFloat)percentage {
    CGFloat computedValue = 0.f;
    
    // TODO: adapt these values to the specific user's previous workout records. Currently the treshhold is 130kg
    
    computedValue = percentage * 130.f / 100.f;
    
    return computedValue;
}

- (void)initializeMuscleGroup:(FTagForMuscleGroupButton)muscleGroup withResistance:(CGFloat)resistenceValue animated:(BOOL)animated {
    if (animated) {
        [self setupImageViewChangeAnimation];
    }
    
    switch (muscleGroup) {
        case kNeckMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-neck"]];
            break;
        case kShouldersMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-shoulders"]];
            break;
        case kBicepsMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-biceps"]];
            break;
        case kTricepsMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-biceps"]];
            break;
        case kAbsMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-abs"]];
            break;
        case kChestMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-chest"]];
            break;
        case kUpperLegsMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-legs"]];
            break;
        case kLowerLegsMuscleGroupTag:
            [self.muscleGroupImageView setImage:[UIImage imageNamed:@"model-calves"]];
            break;
        default:
            break;
    }
    
    [UIView animateWithDuration:animated ? 1.f : 0.f animations:^{
        [self.circularSlider setCurrentValue:resistenceValue];
    }];
}

#pragma mark - Fingerprint Authentication

- (void)checkLocalAuthenticationStatus {
    // Get the local authentication context:
    LAContext *context = [[LAContext alloc] init];
    // Test if fingerprint authentication is available on the device and a fingerprint has been enrolled.
    if ([context canEvaluatePolicy: LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]) {
        [self authenticate];
    }
}

- (void)authenticate {
    LAContext *context = [[LAContext alloc] init];
    
    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"Please authenticate to start your workout." reply:^(BOOL success, NSError *authenticationError) {

        if (success) {
            NSLog(@"Fingerprint validated.");
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:@"Good morning, Dr. Gordon Freeman!"];
                [self.welcomeLabel setText:@"Dr. Gordon Freeman"];
            });
        } else {
            NSLog(@"Fingerprint validation failed: %@.", authenticationError.localizedDescription);
        }
    }];
}

#pragma mark - Button Actions

- (IBAction)muscleGroupButtonPressed:(UIButton *)sender {
    [self initializeMuscleGroup:(FTagForMuscleGroupButton)sender.tag withResistance:100.f/sender.tag animated:YES];
}
@end
